var data = {
    todo: [],
    completed: []
};

// console.log( localStorage.getItem('todoList'));

// Remove and Complete icons in the SVG format

var removeSVG = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22"><path class="fill" d="m16.1 3.6h-1.9v-0.3c0-1.3-1-2.3-2.3-2.3h-1.7c-1.3 0-2.4 1-2.4 2.3v0.2h-1.9c-1.3 0-2.3 1-2.3 2.3v1.3c0 0.5 0.4 0.9 0.9 1v10.5c0 1.3 1 2.3 2.3 2.3h8.5c1.3 0 2.3-1 2.3-2.3v-10.4c0.5-0.1 0.9-0.5 0.9-1v-1.3c-0.1-1.3-1.1-2.3-2.4-2.3zm-7-0.3c0-0.6 0.5-1.1 1.1-1.1h1.7c0.6 0 1.1 0.5 1.1 1.1v0.2h-3.9v-0.2zm7.2 15.4c0 0.6-0.5 1.1-1.1 1.1h-8.5c-0.6 0-1.1-0.5-1.1-1.1v-10.5h10.6l0.1 10.5zm0.9-11.7h-12.4v-1.1c0-0.6 0.5-1.1 1.1-1.1h10.2c0.6 0 1.1 0.5 1.1 1.1v1.1z"/><path class="fill" d="m11 18c-0.4 0-0.6-0.3-0.6-0.6v-6.8c0-0.4 0.3-0.6 0.6-0.6s0.6 0.3 0.6 0.6v6.8c0 0.3-0.2 0.6-0.6 0.6z"/><path class="fill" d="M8 18c-0.4 0-0.6-0.3-0.6-0.6v-6.8C7.4 10.2 7.7 10 8 10c0.4 0 0.6 0.3 0.6 0.6v6.8C8.7 17.7 8.4 18 8 18z"/><path class="fill" d="m14 18c-0.4 0-0.6-0.3-0.6-0.6v-6.8c0-0.4 0.3-0.6 0.6-0.6 0.4 0 0.6 0.3 0.6 0.6v6.8c0 0.3-0.3 0.6-0.6 0.6z"/></svg>';

var completeSVG = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22"><rect class="noFill" width="22" height="22"/><path class="fill" d="M9.7 14.4L9.7 14.4c-0.2 0-0.4-0.1-0.5-0.2l-2.7-2.7c-0.3-0.3-0.3-0.8 0-1.1s0.8-0.3 1.1 0l2.1 2.1 4.8-4.8c0.3-0.3 0.8-0.3 1.1 0s0.3 0.8 0 1.1l-5.3 5.3C10.1 14.3 9.9 14.4 9.7 14.4z"/></svg>';

// User clicked on the add button
// If there is any text inside the item field, add that text to the todo list 
document.getElementById('add').addEventListener('click', function() {
    // console.log('Button clicked');
    let value = document.getElementById('item').value;
    
    if (value) {
        addItem(value);
    }   
});

document.getElementById('item').addEventListener('keydown', function(e) {
    let value = this.value;

    if(e.code === 'Enter' && value) {
        addItem(value);      
    };    
});

function addItem(value) {
    addItemToDOM(value);
    document.getElementById('item').value = '';

    data.todo.push(value);
    dataObjectUpdated();
}

function dataObjectUpdated() {
    // localStorage.setItem('todoList', JSON.stringify(data));
    
    // console.log(JSON.stringify(data));    
    // console.log(data);
}

function removeItem() {
    let item = this.parentNode.parentNode;
    let parent = item.parentNode;
    let id = parent.id;

    let innerPgrphTxt = item.childNodes[0].innerText;

    if (id === 'todo') {
        data.todo.splice(data.todo.indexOf(innerPgrphTxt), 1);
    } else {
        data.completed.splice(data.completed.indexOf(innerPgrphTxt), 1);
    }
    dataObjectUpdated();
    
    parent.removeChild(item);
}

function completeItem() {
    let item = this.parentNode.parentNode;
    let parent = item.parentNode;
    let id = parent.id;

    let innerPgrphTxt = item.childNodes[0].innerText;

    // console.log(innerPgrphTxt);    

    if (id === 'todo') {
        data.todo.splice(data.todo.indexOf(innerPgrphTxt), 1);
        data.completed.push(innerPgrphTxt);        
    } else {
        data.completed.splice(data.completed.indexOf(innerPgrphTxt), 1);
        data.todo.push(innerPgrphTxt);
    }

    dataObjectUpdated();

    // Check if the item should be added to completed list or re-added to todo list
    let target = (id === "todo") ? document.getElementById('completed') : document.getElementById('todo');

    parent.removeChild(item);
    target.insertBefore(item, target.childNodes[0]);
}

// Adds a new item to the todo list
function addItemToDOM(text) {

    let list = document.getElementById('todo');

    let item = document.createElement('li');
    item.innerHTML = '<p>' + text + '<p>';

    let buttons = document.createElement('div');
    buttons.classList.add('buttons');

    let remove = document.createElement('button');
    remove.classList.add('remove');
    remove.innerHTML = removeSVG;

    // Add click event for removing the item
    remove.addEventListener('click', removeItem);

    let complete = document.createElement('button');
    complete.classList.add('complete');
    complete.innerHTML = completeSVG;

    // Add click event for completing the item
    complete.addEventListener('click', completeItem);

    buttons.appendChild(remove);
    buttons.appendChild(complete);
    item.appendChild(buttons);

    list.insertBefore(item, list.childNodes[0]);
}

// Script for Design Customization

document.querySelector('#light-selector').addEventListener('click', function () {
    console.log(this.classList);
    // this.classList.add('dark-applied');

    if (this.classList.contains('dark-applied')) {        
        this.classList.remove('dark-applied');
        
        let lightBg = '#edf0f1';
        let lightItem = '#ffffff';
        let lightText = '#000000';

        document.querySelector('body').style.background = lightBg;
        document.querySelector('html').style.background = lightBg;
        
        // document.querySelector('header button').style.background = darkBg;
        
        document.querySelectorAll('li').forEach(
            (el) => {
                el.style.background = lightItem;
            });

        document.querySelectorAll('li p').forEach(
            (ele) => {
            ele.style.color = lightText;
        });

    } else {
        this.classList.add('dark-applied');

        let darkBg = '#2d2d2d';
        let darkItem = '#000000';
        let darkText = '#8a8a8a';

        document.querySelector('body').style.background = darkBg;
        document.querySelector('html').style.background = darkBg;

        // document.querySelector('header button').style.background = darkBg;

        document.querySelectorAll('li').forEach(
            (el) => {el.style.background = darkItem;
        });

        document.querySelectorAll('li p').forEach(
            (ele) => {ele.style.color = darkText;
        });
    }   
    // console.log(this.classList.contains('dark-applied'));
    // console.log(this.classList.contains('dark-appleta'));    
});

// Violet Color --------------------------------------------------------------------------------------

document.querySelector('#green-item').addEventListener('click', function () {
    let baseColor = '#25b99a';
    let checkColor = '1.5px solid #25b99a';
    let hoverOpacity = 'rgba(37, 185, 154, 0.6)';    

    document.querySelector('header').style.background = baseColor;

    document.querySelectorAll('header button svg .fill').forEach(
        (e) => {
            e.style.fill = baseColor;
        });

    document.querySelectorAll('#completed li .buttons button.complete svg').forEach(
        (el) => {
            el.style.background = baseColor;
        });

    document.querySelectorAll('#todo li .buttons button.complete svg .fill').forEach(
        (ele) => {
            ele.style.fill = baseColor;
        });

    document.querySelectorAll('#completed li .buttons button.complete svg .fill').forEach(
        (elem) => {
            elem.style.fill = '#ffffff';
        });

    document.querySelectorAll('ul.todo li .buttons button.complete svg').forEach(
        (eleme) => {
            eleme.style.border = checkColor;
        });

    // document.querySelectorAll('ul.todo:not(#completed) li .buttons button.complete:hover svg').forEach(
    //     (elemen) => {
    //         elemen.style.background = hoverOpacity;
    //     });
});

document.querySelector('#violet-item').addEventListener('click', function(){
    let baseColor = '#ae48ef';
    let checkColor = '1.5px solid #ae48ef';
    let hoverOpacity = 'rgba(174, 72, 239, 0.6)';

    document.querySelector('header').style.background = baseColor;

    document.querySelectorAll('header button svg .fill').forEach(
        (e) => {
            e.style.fill = baseColor;
    });

    document.querySelectorAll('#completed li .buttons button.complete svg').forEach(
        (el) => {
            el.style.background = baseColor;
    });

    document.querySelectorAll('#todo li .buttons button.complete svg .fill').forEach(
        (ele) => {
            ele.style.fill = baseColor;
    });

    document.querySelectorAll('#completed li .buttons button.complete svg .fill').forEach(
        (elem) => {
            elem.style.fill = '#ffffff';
        });
    
    document.querySelectorAll('ul.todo li .buttons button.complete svg').forEach(
        (eleme) => {
            eleme.style.border = checkColor;
    });

    // document.querySelectorAll('ul.todo:not(#completed) li .buttons button.complete:hover svg').forEach(
    //     (elemen) => {
    //         elemen.style.background = hoverOpacity;
    //     });
});

document.querySelector('#blue-item').addEventListener('click', function () {
    let baseColor = '#256eb9';
    let checkColor = '1.5px solid #256eb9';
    let hoverOpacity = 'rgba(37, 110, 185, 0.6)';

    document.querySelector('header').style.background = baseColor;

    document.querySelectorAll('header button svg .fill').forEach(
        (e) => {
            e.style.fill = baseColor;
        });

    document.querySelectorAll('#completed li .buttons button.complete svg').forEach(
        (el) => {
            el.style.background = baseColor;
        });

    document.querySelectorAll('#todo li .buttons button.complete svg .fill').forEach(
        (ele) => {
            ele.style.fill = baseColor;
        });

    document.querySelectorAll('#completed li .buttons button.complete svg .fill').forEach(
        (elem) => {
            elem.style.fill = '#ffffff';
        });

    document.querySelectorAll('ul.todo li .buttons button.complete svg').forEach(
        (eleme) => {
            eleme.style.border = checkColor;
        });

    // document.querySelectorAll('ul.todo:not(#completed) li .buttons button.complete:hover svg').forEach(
    //     (elemen) => {
    //         elemen.style.background = hoverOpacity;
    //     });
});